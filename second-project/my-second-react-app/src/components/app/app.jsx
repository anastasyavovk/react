import React from 'react';
import { Title } from '../title/title';
import "./app.css"
import { data } from "../data/data";
import Info from '../card/info/info';

function App() {
    return(
        <>
        <Title></Title>
        <Info info = {data}></Info>
        </>
    )
}

export default App;