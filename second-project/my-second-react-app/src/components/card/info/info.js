import React from "react";
import "./info.css";

function Info({info}){
    return(
<div className="container">  
            {
    info.map((el) => {
        const { name,
            image,
            height,
            mass,
            hair_color,
            skin_color,
            eye_color,
            birth_year,
            gender
        } = el;

        return (
        <>
            <ul className="details"> 
                <li className="details-title">{name}</li>
                <li className="details-img"><img src={image} alt="photo"/></li>
                <li>Height: {height}</li>
                <li>Mass: {mass}</li>
                <li>Hair: {hair_color}</li>
                <li>Skin: {skin_color}</li>
                <li>Eye: {eye_color}</li>
                <li>Birth: {birth_year}</li>
                <li>Gender: {gender}</li>
                <li className="btn">
                    <a className="btn-link" href="{url}" target={"_blank"}>Details</a>
                </li>
            </ul>
        </>
        )
})
            }
</div>
    )
}

export default Info;