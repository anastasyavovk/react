import React from "react";
import "./card.css";
import Info from "./info";

// function Card({card}) {

// function Card({info}) {
//     return (
//       <div className="cards">
//         {
//           Array.isArray(info) ? info.map((el) => {
//             return <Card info={el} />
//           }) : null
//         }      
//       </div>
//     )
//   }
  
//   export default Card;

function Card({info}) {
    return (
      <div className="cards">
        {
          Array.isArray(info) ? info.map((el) => {
            return <Card info={el} />
          }) : null
        }      
      </div>
    )
  }
  
  export default Card;