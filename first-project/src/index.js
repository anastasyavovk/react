import {React, Component} from "react";
import ReactDOM from "react-dom";
import data from "./data";
import styles from './index.css'

function App (){
    return(
        <div class="tbl">
        <h1>Курс валют</h1>
        <table border={1}>
        <thead>
          <tr>
            <th>Назва валюти</th>
            <th>Обмінний курс</th>
          </tr>
        </thead>
        <tbody>
          {data.map(element => {
            return (
              <tr key={element.r030}>
                <td>
                  {element.txt}
                </td>
                <td className="price">
                  {element.rate}
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
    )
}
ReactDOM.render(<App></App>, document.getElementById("body"))
console.dir(data)